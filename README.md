# OpenWrt Overlay

This overlay contains several ongoing projects by [Lucas Ramage](https://lramage94.gitlab.io).

## Automated Telephony Testing

- Phoronix-test-suite : [pull request](https://github.com/phoronix-test-suite/phoronix-test-suite/pull/203)

- Package : [utils/phoronix-test-suite](https://gitlab.com/lramage94/openwrt-packages/commit/051585fd275c03e149dd39de51487596ad464764)

See also

- [SIPp](http://sipp.sourceforge.net)

## OpenNIC Integration

- DNS updater : [contrib/package/opennic-up](https://gitlab.com/lramage94/luci/issues/1)

- Dynamic DNS updater : [applications/luci-app-ddns](https://gitlab.com/lramage94/luci/issues/2)

## μvmm

Micro Virtual Machine Manager is a tiny x86_64 hypervisor built for running virtual/containerized network appliances.

"Make it Work, Make it Small, Make it Fast" - Adapted from a saying by [Kent Beck](http://wiki.c2.com/?MakeItWorkMakeItSmallMakeItFast)

Proof of Concept Appliances

- Firewall : [opnsense](https://github.com/opnsense)

- Home Automation : [openhab](https://www.openhab.org)

- Telephony : [2600hz](https://www.2600hz.org)


See also

- [Alpine](https://alpinelinux.org)

- [OpenBSD](https://www.openbsd.org)

References

- [Dnsmasq w/ libvirt](https://wiki.libvirt.org/page/Libvirtd_and_dnsmasq)

- [libvirt domain](https://libvirt.org/formatdomain.html)

- [libvirt networking](https://wiki.libvirt.org/page/Networking)

- [Open vSwitch w/ libvirt](http://docs.openvswitch.org/en/latest/howto/libvirt)